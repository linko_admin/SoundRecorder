package com.linko.soundrecorder;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class SideslipFragment extends Fragment {
    public static final int ITEM_FIELS = 0;
    public static final int ITEM_SETTINGS = 1;
    public static final int ITEM_ABOUT = 2;
    public static final int ITEM_FEEDBACK = 3;

    final int radioIds[] = {
            R.id.item1,
            R.id.item2,
            R.id.item3,
            R.id.item4,
    };
    View items[] = new View[radioIds.length];
    View.OnClickListener clickItem = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            for (int i = 0; i < items.length; ++i) {
                if (v.equals(items[i])) {
                    selectItem(i);
                }
            }
        }
    };
    private DrawerLayout mDrawerLayout;
    private View mFragmentContainerView;
    private NavigationDrawerCallbacks mCallbacks;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Indicate that this fragment would like to influence the set of
        // actions in the action bar.
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_sideslip,
                container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view,
                              Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        findViews();
    }

    private void findViews() {

        for (int i = 0; i < radioIds.length; ++i) {
            items[i] = getView().findViewById(radioIds[i]);
            items[i].setOnClickListener(clickItem);
            // now do not display about author ui.
            if (i == ITEM_ABOUT) {
                items[i].setVisibility(View.GONE);
            }
        }
    }

    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

    }

    private void selectItem(int position) {
        if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerItemSelected(position);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    "Activity must implement NavigationDrawerCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    public static interface NavigationDrawerCallbacks {
        void onNavigationDrawerItemSelected(int position);
    }

}

package com.linko.soundrecorder;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;

import com.umeng.fb.FeedbackAgent;
import com.umeng.fb.fragment.FeedbackFragment;

/**
 * Created by zhongrz on 15-4-9.
 */
public class FeedbackActivity extends FragmentActivity {
    private FeedbackFragment mFeedbackFragment;
    private FeedbackAgent mFeedbackAgent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_feedback);
        mFeedbackAgent = new FeedbackAgent(this);
        mFeedbackAgent.startFeedbackActivity();
        String conversation_id = mFeedbackAgent.getDefaultConversation().getId();
        mFeedbackFragment = FeedbackFragment.newInstance(conversation_id);
        getSupportFragmentManager().beginTransaction().add(R.id.feedback_container, mFeedbackFragment).commit();
        initActionBar();
        mFeedbackAgent.sync();
    }

    public void initActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.title_bar_bg_color));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }
}
